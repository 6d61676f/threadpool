#!/usr/bin/env bash
exe="./src/main"
output="./valgrind_output"
no=50
failed=0

if [ ! -x "$exe" ]; then
    printf "%s not found\n" "$exe"
    exit 1
fi

date > "$output"

for((i=0 ; i<no ; i++)); do
    "$exe" >/dev/null 2>&1
    [ $? -ne 10 ] && ((failed++))
    printf "\n%d\n" $i >> "$output"
    valgrind --tool=memcheck --leak-check=full --show-leak-kinds=all --log-file="$output" "$exe" > /dev/null 2>&1
    printf "i:%d fail:%d\n" $i $failed
done

printf "%d out of %d have failed\n" $failed $no

