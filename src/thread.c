/*! @file thread.c
 *  @brief Implementation file of the thread pool API
 *  @details All API functions of the thread pool are implemented below. Debug
 * functions are in a different file.
 *  @warning Please use with care as it is for educational purposes.
 *  @author Sirbu Laurentiu
 *  @copyright GNU GPLv3
 */

#include "thread.h"
#include "debug.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/*! Internal variable used to control the workers' loops*/
static volatile sig_atomic_t RUNNING = 1;

/*! Each worker has a pointer to his own payload.\n
 * The payload structure is not available directly for the user.
 */
struct thread_payload
{
  /*! Worker state*/
  uint8_t my_state;
  /*! Worker Job Id*/
  uint32_t jid;
  /*! Pointer to worker start function*/
  thread_start_fptr start;
  /*! Start arguments that will be passed to the start_fptr*/
  void* start_args;
  /*! Pointer to worker callback function*/
  thread_callback_fptr callback;
  /*! Callback args that will be passed to the callback_ftpr*/
  void* callback_args;
};

/*! This structure pointer is the handler given to the user.
 * Whatever manipulation is to be made using the API functions.
 */
struct thread_pool
{
  /*! Size of thread pool i.e. number of workers*/
  uint8_t th_size;
  /*! Array containing the worker threads*/
  pthread_t* th_arr;
  /*! Thread pool mutex used for sensitive operations*/
  pthread_mutex_t tp_mutex;
  /*! Array of workers payloads -- each workers uses his own payload*/
  struct thread_payload* payloads;
};

/*! Internal function used for heap deallocation
 * @param[in] tp  Thread pool structure pointer that shall be freed
 * @param[in] freeHandler Also call free on the threadPool_p handler. {0 - no |
 * other - yes}.
 * @return  Number of elements that were NULL
 *      - 3  - Thread pool pointer is NULL
 *      - 2  - Payloads and Worker array are NULL
 *      - 1  - Payloads or Worker array are NULL
 *      - 0  - Everything has been freed as expected. Also applies for
 * freeHandler(0). Then you need to manually free the handler.
 */
static inline uint_fast8_t
free_tp(threadPool_p tp, int8_t freeHandler)
{
  uint_fast8_t ok = 3;
  if (!tp) {
    DEBUG_PRINT("Thread pool is NULL%c", '.');
    return ok;
  }
  if (tp->payloads) {
    free(tp->payloads);
    tp->payloads = NULL;
    ok--;
  }
  if (tp->th_arr) {
    free(tp->th_arr);
    tp->th_arr = NULL;
    ok--;
  }
  if (freeHandler) {
    free(tp);
  }
  return (--ok);
}

/* Note  that  in  order  to  ensure  that  generated signals are queued and
 * signal values passed to sigqueue() are available in si_value, applications
 * which use sigwaitinfo() or sigtimedwait() need to  set  the SA_SIGINFO flag
 * for each signal in the set (see Section 2.4, Signal Concepts).
 */
/*!Internal signal handler function user by the thread pool in order to issue
 * and handle commands
 * @param[in] signo     Signal number caught
 * @param[in] info      Pointer to singinfo_t structure
 * @param[in] context   Pointer to signal stack context
 */
static void
handle_signal(int signo, siginfo_t* info, void* context)
{
  switch (signo) {
    case RTK1:
      RUNNING = 0;
      break;
    case RTK2:
      break;
    case RTK3:
    default:
      DEBUG_PRINT("Killed ERRNO %d CAUSE %d", info->si_errno, info->si_code);
      pthread_exit(NULL);
      break;
  }
}

/*! Internal function used to unblock the signals in each worker.
 *  @param[in,out] blocking Sigset_t that shall be initialized with blocked
 * signals.
 *  @return
 *      - EXIT_FAILURE
 *      - EXIT_SUCCESS
 */
static int_fast8_t
threadSignalUnblock(sigset_t* blocking)
{
  if (!blocking) {
    DEBUG_PRINT("sigset_t pointer is NULL%c", '.');
    return EXIT_FAILURE;
  }
  sigemptyset(blocking);
  sigaddset(blocking, RTK1);
  sigaddset(blocking, RTK2);
  sigaddset(blocking, RTK3);
  if (pthread_sigmask(SIG_UNBLOCK, blocking, NULL) != 0) {
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

/*! Internal function used initially to kickstart the workers via pthread_create
 *  @param[in,out] args Pointer to worker's payload structure
 *  @return
 *      - NULL because the workers start as detached
 */
static void*
thread_entry(void* args)
{
  if (!args) {
    DEBUG_PRINT("Really bad. We shouldn't get here%lu", pthread_self());
    pthread_exit(NULL);
  }
  struct thread_payload* load = (struct thread_payload*)args;
  void* result = NULL;
  sigset_t waiting;
  siginfo_t received_signal;
  if (threadSignalUnblock(&waiting) != EXIT_SUCCESS) {
    load->my_state = TERM;
    pthread_exit(NULL);
  }
  while (RUNNING) {
    switch (load->my_state) {
      case INIT: {
        load->my_state = FREE;
        DEBUG_PRINT("Done initializing %lu", pthread_self());
        break;
      }
      case FREE: {
        if (sigwaitinfo(&waiting, &received_signal) > 0) {
          DEBUG_PRINT(
            "Received signal %s %lu",
            (received_signal.si_signo == RTK1)
              ? "RTK1"
              : ((received_signal.si_signo == RTK2) ? "RTK2" : "RTK3"),
            pthread_self());
        }
        break;
      }
      case WORK: {
        DEBUG_PRINT("Starting work %lu", pthread_self());
        if (load->start) {
          DEBUG_PRINT("Entering start %lu", pthread_self());
          result = load->start(load->start_args);
        }
        if (load->callback) {
          DEBUG_PRINT("Entering callback %lu", pthread_self());
          load->callback(load->callback_args, result);
        }
        if (load->my_state == STOP) {
          break;
        }
        DEBUG_PRINT("Free again %lu", pthread_self());
        load->my_state = FREE;
        result = NULL;
        break;
      }
      case STOP:
      default: {
        DEBUG_PRINT("Stopping %lu", pthread_self());
        load->my_state = TERM;
        DEBUG_PRINT("About to exit %lu", pthread_self());
        pthread_exit(NULL);
        break;
      }
    }
  }
  DEBUG_PRINT("About to exit %lu", pthread_self());
  load->my_state = TERM;
  pthread_exit(NULL);
}

/*! Internal function used to generate the worker_id handler.
 * The least significant byte is the state, the next one is the array index,
 * the next 32 bits are the job id and the last 16 bits are reserved.
 * @param jid   Unique Job Id
 * @param index Pthread_t array index
 * @param state Worker state - should be WORK
 * @return
 *      - A valid worker_id
 */
static inline worker_id
fill_worker_id(uint32_t jid, uint8_t index, uint8_t state)
{
  worker_id vid = 0;
  vid = ((worker_id)jid << 16 | (worker_id)index << 8 | (worker_id)state);
  DEBUG_PRINT("Received index(%#x) state(%#x) => id(%#" PRIxFAST64 ")",
              index,
              state,
              vid);
  return vid;
}

/*! Internal function used to setup the signal configuration.\n
 * All the USER_SIGNALS used for communication with the workers start as blocked
 * in the controller thread.\n
 * Every worker after being initialized unblocks the signals.
 * @return
 *      - EXIT_FAILURE
 *      - EXIT_SUCCESS
 */
static uint_fast8_t
masterSignalInit(void)
{
  sigset_t block_usr;
  struct sigaction act;
  act.sa_flags = SA_SIGINFO;
  act.sa_sigaction = handle_signal;
  sigemptyset(&act.sa_mask);
  sigemptyset(&block_usr);
  sigaddset(&block_usr, RTK1);
  sigaddset(&block_usr, RTK2);
  sigaddset(&block_usr, RTK3);
  pthread_sigmask(SIG_BLOCK, &block_usr, NULL);
  if (sigaction(RTK1, &act, NULL) != 0) {
    returnMessage(EXIT_FAILURE, "Failed at sigaction(%s).", "RTK1");
  }
  if (sigaction(RTK2, &act, NULL) != 0) {
    returnMessage(EXIT_FAILURE, "Failed at sigaction(%s).", "RTK2");
  }
  if (sigaction(RTK3, &act, NULL) != 0) {
    returnMessage(EXIT_FAILURE, "Failed at sigaction(%s).", "RTK3");
  }
  return EXIT_SUCCESS;
}

threadPool_p
initialize_pool(uint8_t size)
{
  srandom(time(NULL));
  int error;
  pthread_attr_t threadAttr;
  struct thread_pool* tp = NULL;

  if (masterSignalInit() != EXIT_SUCCESS) {
    failMessage(NULL, "Signal initialization has failed%c", '.');
  }
  if ((error = pthread_attr_init(&threadAttr))) {
    returnMessage(NULL, "Failed at %s(%d).", "pthread_attr_init", error);
  }
  if ((error =
         pthread_attr_setdetachstate(&threadAttr, PTHREAD_CREATE_DETACHED))) {
    pthread_attr_destroy(&threadAttr);
    returnMessage(NULL, "Failed at %s(%d).", "pthread_attr_setdetach", error);
  }

  tp = (struct thread_pool*)calloc(1, sizeof *tp);
  if (!tp) {
    pthread_attr_destroy(&threadAttr);
    failMessage(NULL, "Thread pool allocation has failed%c", '.');
  }
  if ((error = pthread_mutex_init(&tp->tp_mutex, NULL)) != 0) {
    free_tp(tp, 1);
    pthread_attr_destroy(&threadAttr);
    failMessage(NULL, "Failed at initializing mutex: %d.", error);
  }
  tp->th_size = size;
  tp->th_arr = (pthread_t*)calloc(size, sizeof *(tp->th_arr));
  tp->payloads = (struct thread_payload*)calloc(size, sizeof *(tp->payloads));
  if (!(tp->th_arr && tp->payloads)) {
    pthread_attr_destroy(&threadAttr);
    pthread_mutex_destroy(&tp->tp_mutex);
    free_tp(tp, 1);
    returnMessage(NULL, "Thread pool allocation has failed%c", '.');
  }
  for (size_t i = 0; i < size; i++) {
    tp->payloads[i].my_state = TERM;
  }
  for (size_t i = 0; i < size; i++) {
    tp->payloads[i].my_state = INIT;
    if ((error = pthread_create(
           &tp->th_arr[i], &threadAttr, thread_entry, &tp->payloads[i])) != 0) {
      tp->payloads[i].my_state = TERM;
      pthread_attr_destroy(&threadAttr);
      kill_pool(&tp);
      failMessage(NULL, "Failed at creating thread[%zu]:%d.", i, error);
    }
  }
  pthread_attr_destroy(&threadAttr);
  for (size_t i = 0; i < tp->th_size; i++) {
    if (pthread_kill(tp->th_arr[i], 0) != 0) {
      kill_pool(&tp);
      failMessage(NULL, "Failed at sanity check for pthread[%zu]", i);
    }
  }
  return (tp);
}

int_fast8_t
kill_pool(threadPool_p* tp)
{
  int8_t tainted = 1;
  if (!(tp && *tp)) {
    returnMessage(-2, "Thread pool is NULL%c", '.');
  } else {
    int8_t counter = 6;
    int error;
    struct thread_pool* ptp = *tp;
    if ((error = pthread_mutex_trylock(&ptp->tp_mutex)) != 0) {
      returnMessage((-1),
                    "Could not acquire lock. Thread pool has "
                    "not been killed:%d",
                    error);
    }
    for (size_t i = 0; i < ptp->th_size; i++) {
      if (ptp->payloads[i].my_state != TERM) {
        ptp->payloads[i].my_state = STOP;
        if ((error = pthread_kill(ptp->th_arr[i], RTK1)) != 0) {
          pthread_mutex_unlock(&ptp->tp_mutex);
          returnMessage(
            (-1),
            "pthread_kill[%zu] has failed. Thread pool has not been killed:%d",
            i,
            error);
        }
      }
    }
    do {
      tainted = 0;
      counter--;
      for (size_t i = 0; i < ptp->th_size; i++) {
        if (!(ptp->payloads[i].my_state & TERM)) {
          tainted = 1;
          nanosleep((struct timespec[]){ { 0, 500000000 } }, NULL);
          break;
        }
      }
    } while (tainted && counter);
    if (tainted) {
      DEBUG_PRINT("%s", "Killing threads..");
      for (size_t i = 0; i < ptp->th_size; i++) {
        if (!(ptp->payloads[i].my_state & TERM)) {
          if ((error = pthread_kill(ptp->th_arr[i], RTK3)) != 0) {
            pthread_mutex_unlock(&ptp->tp_mutex);
            returnMessage((-1),
                          "pthread_kill[%zu] has failed. Thread pool has not "
                          "been killed:%d",
                          i,
                          error);
          }
          ptp->payloads[i].my_state = TERM;
        }
      }
    }
    free_tp(ptp, 0);
    *tp = NULL;
    pthread_mutex_unlock(&ptp->tp_mutex);
    pthread_mutex_destroy(&ptp->tp_mutex);
    free(ptp);
  }
  return tainted;
}

worker_id
assign_task(threadPool_p tp,
            thread_start_fptr start,
            void* start_args,
            thread_callback_fptr callback,
            void* callback_args)
{
  int error;
  if (!tp) {
    returnMessage(0, "Thread pool is NULL%c", '.');
  }
  if ((error = pthread_mutex_trylock(&tp->tp_mutex)) != 0) {
    returnMessage(1,
                  "Could not acquire lock. Thread pool has "
                  "not been killed:%d",
                  error);
  }
  for (size_t i = 0; i < tp->th_size; i++) {
    if (tp->payloads[i].my_state == FREE) {
      tp->payloads[i].start = start;
      tp->payloads[i].start_args = start_args;
      tp->payloads[i].callback = callback;
      tp->payloads[i].callback_args = callback_args;
      tp->payloads[i].my_state = WORK;
      tp->payloads[i].jid = random();
      if ((error = pthread_kill(tp->th_arr[i], RTK2)) != 0) {
        pthread_mutex_unlock(&tp->tp_mutex);
        returnMessage(1,
                      "pthread_kill[%zu] has failed. Could not assign task:%d",
                      i,
                      error);
      }
      pthread_mutex_unlock(&tp->tp_mutex);
      DEBUG_PRINT("Assigned successfully to %lu : %#x",
                  tp->th_arr[i],
                  tp->payloads[i].jid);
      return fill_worker_id(tp->payloads[i].jid, i, 0xFF);
    }
  }
  pthread_mutex_unlock(&tp->tp_mutex);
  DEBUG_PRINT("Could not assign task. No free workers%c", '.');
  return 2;
}

int_fast8_t
wait_pool(threadPool_p tp, int32_t iterations, int8_t stop)
{
  if (!tp) {
    returnMessage(-1, "Thread pool is NULL%c", '.');
  } else {
    int8_t tainted = 1;
    if (stop) {
      DEBUG_PRINT("%s", "Canceling loop before waiting");
      RUNNING = 0;
    }
    DEBUG_PRINT("%s", "Waiting for workers");
    do {
      tainted = 0;
      iterations--;
      for (size_t i = 0; i < tp->th_size; i++) {
        if (tp->payloads[i].my_state & (WORK | INIT)) {
          tainted = 1;
          nanosleep((struct timespec[]){ { 0, 500000000 } }, NULL);
          break;
        }
      }
    } while (tainted && iterations);
    DEBUG_PRINT("%s %s",
                "Done waiting for workers -- ",
                (tainted) ? "still working" : "finished");
    return tainted;
  }
}

/*! Internal function that checks if worker_id is valid
 * @param[in] id Worker id
 * @param[in] tp Thread pool structure pointer
 * @return
 *      - EXIT_FAILURE
 *      - EXIT_SUCCESS
 */
static inline int_fast8_t
check_worker_id(const threadPool_p tp, worker_id id)
{
  if (id == 0) {
    returnMessage(EXIT_FAILURE, "Worker id is %d", 0);
  }
  if (tp) {
    const uint32_t jid = (id >> 16);
    const uint8_t index = (id >> 8);
    DEBUG_PRINT("%s%#" PRIxFAST64 ")",
                (((uint8_t)id == 0xff) ? "Initial check for worker("
                                       : "Not the first call for worker("),
                id);
    if (index >= tp->th_size) {
      returnMessage(EXIT_FAILURE, "Worker id is out of bounds%c", '.');
    }
    if (tp->payloads[index].jid != jid) {
      returnMessage(EXIT_FAILURE,
                    "Job has not been found(%#x != %#x). It probably has "
                    "finished already.",
                    tp->payloads[index].jid,
                    jid);
    }
    return EXIT_SUCCESS;
  } else {
    returnMessage(EXIT_FAILURE, "Thread pool is NULL%c", '.');
  }
}

worker_id
check_worker(const threadPool_p tp, worker_id id)
{
  const uint8_t index = id >> 8;
  const uint32_t jid = id >> 16;
  uint8_t state;
  if (!tp) {
    returnMessage(0, "Thread pool structure pointer is NULL%c", '.');
  }
  if (check_worker_id(tp, id) != EXIT_SUCCESS) {
    returnMessage(0, "Worker id is invalid%c", '.');
  }
  state = tp->payloads[index].my_state;
  return fill_worker_id(jid, index, state);
}
