#include "thread.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#ifndef TS
#define TS 10
#endif

void*
count_to_ten(void)
{
  void* ceva = malloc(2000);
  printf("\nLet's count\n");
  for (int i = 1; i <= 10; i++) {
    printf("%d ", i);
  }
  printf("\nReturning...%lu\n", pthread_self());
  sleep(2);
  return ceva;
}

void
free_space(void* opt, void* mem)
{
  if (opt) {
    *(int*)opt = 0x0;
  }
  if (mem) {
    free(mem);
  }
}

int
main(void)
{
  threadPool_p tp = initialize_pool(TS % 256);
  if (!tp) {
    return EXIT_FAILURE;
  }
  worker_id id = 0, id_valid = 0;
  wait_pool(tp, 0, 0);
  for (size_t j = 0; j < TS / 2 + 1 && tp; j++) {
    for (size_t i = 0; i < TS + 10 && tp; i++) {
      id = assign_task(tp,
                       (thread_start_fptr)count_to_ten,
                       0,
                       (thread_callback_fptr)free_space,
                       0);
      if (id >= 0xFF) {
        printf("Found valid id %#lx\n", id);
        id_valid = id;
      }
    }
    printf("\nDone assigning\n");
    do {
      id_valid = check_worker(tp, id_valid);
      printf("Last VALID assigned task(%#" PRIxFAST64
             ") has state:%#" PRIxFAST64 "\n",
             id_valid,
             id_valid & 0xFF);
      sleep(1);
    } while (!(id_valid & FREE) && !!id_valid);
  }
  wait_pool(tp, 0, 0);
  kill_pool(&tp);
  return 10;
}
