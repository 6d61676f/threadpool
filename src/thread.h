/*! @file thread.h
 *  @brief Thread pool API
 *  @details These functions are to be used with the thread pool handler in
 * order to issue commands
 *  @warning Please use with care as it is for educational purposes.
 *  @author Sirbu Laurentiu
 *  @copyright GNU GPLv3
 */

#ifndef THREAD_POOL
#define THREAD_POOL
#include <inttypes.h>
#include <signal.h>

/*! Forward declaration of thread_pool structure*/
struct thread_pool;

/*! Thread pool handler used by the controlling thread i.e. user.*/
typedef struct thread_pool* threadPool_p;

/*! Worker id consisting of an int where the least significant byte is the state
 * and the next is the array position.\n
 * The complete format is:\n
 * <b>0xRE:SV:RA:ND:RA:ND:index:state</b>, where RAND stands for a random ID and
 * RESV is reserved for future usage
 */
typedef uint_fast64_t worker_id;

/*! Prototype of the worker start function.\n
 * Each worker handles work using this function interface.
 * @param start_args  User passed argument
 */
typedef void* (*thread_start_fptr)(void* start_args);

/*! [OPTIONAL]Prototype of the worker callback function.\n
 * The user can pass a callback function that the worker can call after
 * finishing the start call.
 * @param user_args  User passed argument
 * @param start_ret_val  The return value of 'thread_start_fptr'
 */
typedef void* (*thread_callback_fptr)(void* user_args, void* start_ret_val);

/*! Enumeration of the possible states a worker can have.
 * @warning It should support a maximum of 8 bits width.
 */
enum THREAD_STATE
{
  /*! Initialization state, used only at the beginning as a check*/
  INIT = (1 << 0),
  /*! Worker is free and waiting for a signal*/
  FREE = (1 << 1),
  /*!
   *  Worker is currently processing 'thread_start_fptr' or
   *  'thread_callback_fptr'
   */
  WORK = (1 << 2),
  /*! Worker has been issued the stop command by the controller thread*/
  STOP = (1 << 3),
  /*! Worker has quit*/
  TERM = (1 << 4)
};

/*!
 *User defined signals through which the controller thread can reach the
 *workers.
 */
enum USER_SIGNALS
{
  /*! Try to gracefully quit by waking up and breaking the while loop*/
  RTK1 = __SIGRTMIN + 2,
  /*! Wake up thread from sleep to process work */
  RTK2,
  /*! Kill thread */
  RTK3
};

/*! Function that creates a thread pool of given size.
 * @param[in] size  Number of workers to initialize
 * @return
 *      - Valid thread pool structure pointer
 *      - NULL
 */
threadPool_p
initialize_pool(uint8_t size);

/*! Function that terminates the execution of the thread pool.\n
 * It tries to gracefully stop the workers and if that does not work it issues a
 * kill command.
 * @param[in,out] tp    Pointer to a thread pool structure pointer
 * @return
 *      - (-2)  - Thread pool structure pointer is NULL
 *      - (-1)  - Pthread_* call error. Check errno.
 *      - 0     - Thread pool has been cleanly terminated
 *      - 1     - Thread pool has been forced to quit
 */
int_fast8_t
kill_pool(threadPool_p* tp);

/*! Function through which to assign work to thread pool.\n
 * Initially the state is set to 0xFF.
 * @param[in] tp             Thread pool structure pointer
 * @param[in] start          Pointer to start function
 * @param[in] start_args     [OPTIONAL] Start args
 * @param[in] callback       [OPTIONAL] Pointer to callback function
 * @param[in] callback_args  [OPTIONAL] Callback args
 * @return
 *      - 0         - Thread pool is NULL.
 *      - 1         - Error from pthread_* call. Check errno.
 *      - 2         - No free workers.
 *      - Job id    - On success. Initially >= 0xFF. Future calls to
 * check_worker sets the correct state.
 */
worker_id
assign_task(threadPool_p tp,
            thread_start_fptr start,
            void* start_args,
            thread_callback_fptr callback,
            void* callback_args);

/*! Function that waits for all workers of thread pool to be free again.
 * @warning Use with great care!
 * @param[in] tp            Thread pool structure pointer
 * @param[in] iterations    The number of 0.5 second iterations to do before
 * returning {<=0 - infinite loop | 1 - immediate check | >1 - # of loops}
 * @param[in] stop          Specifies if worker state loop is to be halted
 * before waiting {0 - false | other - true}
 * @return
 *      - (-1)  - Thread pool structure pointer is NULL
 *      -  0    - All workers are free
 *      -  1    - There are workers still in action
 */
int_fast8_t
wait_pool(threadPool_p tp, int32_t iterations, int8_t stop);

/*! Function that checks the state of a given task.
 * @param[in] tp Valid thread pool structure pointer
 * @param[in] id Valid worker_id generated initially by assign_task
 * @return
 *      - 0             - Something went wrong (NULL pointers or corrupt id)
 *      - Worker Id     - Updated worker ID. This one should be used on next
 * calls.
 */
worker_id
check_worker(const threadPool_p tp, worker_id id);

#endif
