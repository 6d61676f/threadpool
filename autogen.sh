#!/usr/bin/env bash
configure_path="$PWD"
autoreconf --install || exit 1
mkdir -p build && cd build || exit 1
debugFlag="-DDEBUG_MESSAGE -DVERBOSE_MESSAGE"
coverageFlag="-fprofile-arcs -ftest-coverage"
CFLAGS="-ggdb3 -O0 -DTS=20 $debugFlag $coverageFlag" LDFLAGS="-ggdb3 $coverageFlag" \
    "$configure_path/configure"
make clean
make
printf "\n\nSource files are in ./src and build files are in ./build/src\n"
